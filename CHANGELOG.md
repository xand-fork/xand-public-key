[[_TOC_]]

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).



## [0.4.0] - 15 July, 2021

### Changed
- Conversions `public_key_to_address()` and `ristretto_point_to_address()` are now infallible.

## [0.3.0] - 12 July 2021

### Changed
- Use `xand_ledger v.0.29.0`

## [0.2.0] - 6 July, 2021

### Added
- Conversions for `address_to_public_key()`, `public_key_to_address()`, `ristretto_point_to_address()` fully ported from `xand_financial_client_adapter`


## Unreleased
<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->

<!-- When a new release is made, add it here
e.g. ## [0.8.2] - 2020-11-09 -->

